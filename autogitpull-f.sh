#!/bin/bash

# Automating git pull scripting

# Configurable Options
#Mail_From="from@domain.com"
#Mail_To="to@domain.com"
#Mail_Cc="cc@domain.com"
#Mail_Subject="Git: New changes detected"

Branch="master"

# Do not change the lines below
Log_Pull="/tmp/cron_git_pull.log"
Log_FetchHead="/tmp/cron_git_fetch.log"

TARGETDIR=$HOME'/tmp/test/'

DIRS=`ls -l $TARGETDIR | egrep "^d" | awk '{print $9}'`

for DIR in $DIRS
do
    DIR=$TARGETDIR$DIR'/'
    
    # Check if exists ssh keys
    if [ ! -f ~/.ssh/id_rsa ] || [ ! -f ~/.ssh/id_rsa.pub ] ; then
        echo "SSH keys not found!"
        echo "Use ssh-keygen command to create key pair"
        echo "More info: https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/"
        exit
    fi

    # Start check using fetch
    git -C "$DIR" fetch origin $Branch
    git -C "$DIR" log -p HEAD..FETCH_HEAD > $Log_FetchHead

    if [[ $(git -C "$DIR" diff origin/$Branch | wc -c) -eq 0 ]]
        then echo "Up-to-date: Nothing to do"
    else
    # Verify differences between the last and new commit
        sm=$(cat $Log_Pull)
        git -C "$DIR" pull origin $Branch > $Log_Pull
        smnew=$(cat $Log_Pull)
        if [ "$sm" == "$smnew" ] || [ "$smnew" == "Already up-to-date." ]
            then echo "Same commit: Nothing to do"
	    else cat $Log_Pull $Log_FetchHead
#            else cat $Log_Pull $Log_FetchHead | mailx -r "$Mail_From" -s "$Mail_Subject - $(date)" -t $Mail_To ", $Mail_Cc"
        fi
    fi

done







