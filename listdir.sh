#!/bin/bash

TMPDIR=$HOME'/tmp/test/'

#echo $MYDIR

DIRS=`ls -l $TMPDIR | egrep "^d" | awk '{print $9}'`

# “ls -l $MYDIR” = get a directory listing
# “| egrep ‘^d’” = pipe to egrep and select only the directories
# “awk ‘{print $8}’” = pipe the result from egrep to awk and print only the 8th field

#echo $DIRS

# and now loop through the directories:
for DIR in $DIRS
do
	DIR=$TMPDIR$DIR'/'
	echo ${DIR}
done
